﻿using AdoptesUnDev.Models;
using AdoptesUnDev.Services;
using Chat.ViewModels;
using Xamarin.Forms;

namespace AdopteUnDev.ViewModels
{
    class RegDevViewModel : ViewModelBase
    {
        private string _FirstName;

        public string FirstName
        {
            get { return _FirstName; }
            set { SetValue(ref _FirstName, value); }
        }

        private string _LastName;

        public string LastName
        {
            get { return _LastName; }
            set { SetValue(ref _LastName, value); }
        }
        private string _City;

        public string City
        {
            get { return _City; }
            set { SetValue(ref _City, value); }
        }

        private string _Login;

        public string Login
        {
            get { return _Login; }
            set { SetValue(ref _Login, value); }
        }

        private string _Password;

        public string Password
        {
            get { return _Password; }
            set { SetValue(ref _Password, value); }
        }




        private Command _RegisterDev;

        public Command RegisterDev
        {
            get { return _RegisterDev = _RegisterDev ?? new Command(RegDev); }

        }

        private void RegDev()
        {

            DependencyService.Get<IRegisterDevService>().Add(new Dev
            {
                FirstName = FirstName,
                LastName = LastName,
                City = City,
                LogInfo = new LoginInfo
                {
                    Login = Login,
                    Password = Password
                }

            });
        }
    }
}
