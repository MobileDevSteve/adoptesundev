﻿using AdoptesUnDev.Models;
using AdoptesUnDev.Services;
using AdoptesUnDev.Views;
using Chat.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace AdoptesUnDev.ViewModels
{
    class WelcomeViewModel : ViewModelBase
    {
        private string _Login;

        public string Login
        {
            get { return _Login; }
            set { SetValue(ref _Login, value); }
        }

        private string _Password;

        public string Password
        {
            get { return _Password; }
            set { SetValue(ref _Password, value); }
        }

        private Command _LoginCMD;
        public Command LoginCMD
        {
            get { return _LoginCMD = _LoginCMD ?? new Command(Logme); }
        }

        private Command _NewDev;

        public Command NewDev
        {
            get { return _NewDev = _NewDev ?? new Command(OpenDevReg); }
        }


        private Command _NewRec;

        public Command NewRec
        {
            get { return _NewRec = _NewRec ?? new Command(OpenRecReg); }
        }

        private List<LoginInfo> ListLog = new List<LoginInfo>();

        private void Logme()
        {
            foreach (var item in ListLog)
            {
                if(item.Login == Login && item.Password == Password)
                {
                    App.Current.Properties.Add("user_key", item.Key);
                }
            }
        }

        public WelcomeViewModel() { LoadItems(); }

        private async void LoadItems()
        {
            var collection = await DependencyService.Get<LoginService>().GetAllAsync();
            ListLog.Clear();
            foreach (var LogInfo in collection)
            {
                ListLog.Add(LogInfo);
            }
        }

        private void OpenDevReg()
        {
            App.Current.MainPage = new ProfilDev();
        }

        private void OpenRecReg()
        {
            App.Current.MainPage = new RecruteurView();
        }

    }
}
