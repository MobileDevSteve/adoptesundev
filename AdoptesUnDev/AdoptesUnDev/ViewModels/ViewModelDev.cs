﻿using AdoptesUnDev.Models;
using AdoptesUnDev.Services;
using Chat.ViewModels;
using Firebase.Database;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace AdoptesUnDev.ViewModels
{
    class ViewModelDev : ViewModelBase
    {
        private string _LastName;

        public string LastName
        {
            get { return _LastName; }
            set { SetValue(ref _LastName, value); }
        }

        private string _FirstName;

        public string FirstName
        {
            get { return _FirstName; }
            set { SetValue(ref _FirstName, value); }
        }

        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { SetValue(ref _Email, value); }
        }

        private string _City;

        public string City
        {
            get { return _City; }
            set { SetValue(ref _City, value); }
        }

        private DateTime _BirthDate;

        public DateTime BirthDate
        {
            get { return _BirthDate; }
            set { SetValue(ref _BirthDate, value); }
        }

        private string _FreeDesc;

        public string FreeDesc
        {
            get { return _FreeDesc; }
            set { SetValue(ref _FreeDesc, value); }
        }

        private string _Login;

        public string Login
        {
            get { return _Login; }
            set { SetValue(ref _Login, value); }
        }

        private string _Password;

        public string Password
        {
            get { return _Password; }
            set { SetValue(ref _Password, value); }
        }


        private Command _SendCommand;

        public Command SendCommand
        {
            get { return _SendCommand = _SendCommand ?? new Command(Send); }
        }

        public ViewModelDev()
        {
            
        }

        private void Send()
        {
            // Changer avec le code du service de Steve
            DependencyService.Get<IRegisterDevService>().Add(new Dev
                {
                    LastName = LastName,
                    FirstName = FirstName,
                    Email = Email,
                    City = City,
                    BirthDate = BirthDate,
                    FreeDesc = FreeDesc,
                    LogInfo = new LoginInfo
                    {
                        Login = Login,
                        Password = Password
                    }
                });
        }

    }
}
