﻿using AdoptesUnDev.Models;
using AdoptesUnDev.Services;
using Chat.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace AdoptesUnDev.ViewModels
{
    class ViewModelRecru : ViewModelBase
    {
        private string _SocietyName;

        public string SocietyName
        {
            get { return _SocietyName; }
            set { SetValue(ref _SocietyName, value); }
        }

        private string _CityWork;

        public string CityWork
        {
            get { return _CityWork; }
            set { SetValue(ref _CityWork, value); }
        }

        private string _ActivitySector;

        public string ActivitySector
        {
            get { return _ActivitySector; }
            set { SetValue(ref _ActivitySector, value); }
        }

        private string _Description;

        public string Description
        {
            get { return _Description; }
            set { SetValue(ref _Description, value); }
        }

        private string _Login;

        public string Login
        {
            get { return _Login; }
            set { SetValue(ref _Login, value); }
        }

        private string _Password;

        public string Password
        {
            get { return _Password; }
            set { SetValue(ref _Password, value); }
        }

        private Command _SendCommand;

        public Command SendCommand
        {
            get { return _SendCommand = _SendCommand ?? new Command(Send); }
        }

        private void Send()
        {
            // Changer avec le code du service de Steve
            DependencyService.Get<IRegisterRecService>().Add(new Recruteur
            {
                SocietyName = SocietyName,
                CityWork = CityWork,
                ActivitySector= ActivitySector,
                Description = Description,
                LogInfo = new LoginInfo
                {
                    Login = Login,
                    Password = Password
                }
            });
        }


    }
}
