﻿using AdoptesUnDev.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdoptesUnDev.Services
{
    interface IRegisterRecService
    {
        void Add(Recruteur dev);
        event Action<Recruteur> OnAdd;
        Task<List<Recruteur>> GetAllAsync();
    }
}
