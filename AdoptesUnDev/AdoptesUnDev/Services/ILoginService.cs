﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdoptesUnDev.Models;

namespace AdoptesUnDev.Services
{
    interface ILoginService
    {
        Task<List<LoginInfo>> GetAllAsync();
    }
}