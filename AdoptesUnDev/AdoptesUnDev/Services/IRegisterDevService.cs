﻿using AdoptesUnDev.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AdoptesUnDev.Services
{
    internal interface IRegisterDevService
    {
        void Add(Dev dev);
        event Action<Dev> OnAdd;
        Task<List<Dev>> GetAllAsync();
    }
}