﻿using AdoptesUnDev.Models;
using Firebase.Database;
using Firebase.Database.Streaming;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AdoptesUnDev.Services
{
    class RegisterDevService : IRegisterDevService

    {
        private FirebaseClient _Client;
        private List<Dev> _Devs = new List<Dev>();


        public RegisterDevService()
        {
            _Client = DependencyService.Get<IFirebaseDBProvider>().GetClient();

            _Client.Child("Devs").AsObservable<Dev>().Subscribe((ev) => {
                if (ev.Object == null)
                { return; }

                Dev dev = _Devs.FirstOrDefault(m => m.Key == ev.Key);
                if (ev.EventType == FirebaseEventType.InsertOrUpdate)
                {
                    if (dev == null)
                    {
                        Dev newD = new Dev
                        {
                            Key = ev.Key,
                            FirstName = ev.Object.FirstName,
                            LastName = ev.Object.LastName,
                            City = ev.Object.City,
                            LogInfo = ev.Object.LogInfo
                        };
                        _Devs.Add(newD);
                        OnAdd?.Invoke(newD);
                    }
                }
            });
        }

        public event Action<Dev> OnAdd;


        public async Task<List<Dev>> GetAllAsync()
        {
            var elems = await _Client.Child("Devs").OnceAsync<Dev>();
            foreach (var item in elems)
            {
                _Devs.Add(new Dev
                {
                    Key = item.Key,
                    FirstName = item.Object.FirstName,
                    LastName = item.Object.LastName,
                    City = item.Object.City,
                    LogInfo = item.Object.LogInfo

                });
            }
            return _Devs;
        }

        public void Add(Dev dev)
        {
            _Client.Child("Devs").PostAsync(JsonConvert.SerializeObject(dev));
            _Devs.Add(dev);
        }
    }
}