﻿using System;
using System.Collections.Generic;
using System.Text;
using Firebase.Database;

namespace AdoptesUnDev.Services
{
    class FirebaseDBProvider : IFirebaseDBProvider
    {
        public FirebaseClient GetClient()
        {
            return new FirebaseClient("https://adoptesundev.firebaseio.com/");
        }
    }
}
