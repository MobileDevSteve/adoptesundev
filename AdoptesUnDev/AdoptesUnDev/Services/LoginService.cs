﻿using AdoptesUnDev.Models;
using Firebase.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AdoptesUnDev.Services
{
    class LoginService : ILoginService
    {
        private FirebaseClient _Client;
        private List<LoginInfo> _LoginInfo = new List<LoginInfo>();

        public LoginService()
        {
            _Client = DependencyService.Get<IFirebaseDBProvider>().GetClient();
        }

        public async Task<List<LoginInfo>> GetAllAsync()
        {
            var lesDevs = await _Client.Child("Devs").OnceAsync<Dev>();
            
            foreach (var item in lesDevs)
            {
                _LoginInfo.Add(new LoginInfo {
                    Key = item.Key,
                    Login = item.Object.LogInfo?.Login,
                    Password = item.Object.LogInfo?.Password
                });
            }

            var lesRecs = await _Client.Child("Recruteurs").OnceAsync<Recruteur>();

            foreach (var item in lesRecs)
            {
                _LoginInfo.Add(new LoginInfo
                {
                    Key = item.Key,
                    Login = item.Object.LogInfo?.Login,
                    Password = item.Object.LogInfo?.Password
                });
            }

            return _LoginInfo;
        }

    }
}
