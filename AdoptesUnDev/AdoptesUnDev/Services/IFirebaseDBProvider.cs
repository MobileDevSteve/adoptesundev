﻿using Firebase.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdoptesUnDev.Services
{
    interface IFirebaseDBProvider
    {
        FirebaseClient GetClient();
    }
}
