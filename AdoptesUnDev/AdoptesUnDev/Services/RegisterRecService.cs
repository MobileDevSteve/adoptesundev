﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdoptesUnDev.Models;
using Firebase.Database;
using Firebase.Database.Streaming;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace AdoptesUnDev.Services
{
    class RegisterRecService : IRegisterRecService
    {

        private FirebaseClient _Client;
        private List<Recruteur> _Rec = new List<Recruteur>();

        public event Action<Recruteur> OnAdd;

        public RegisterRecService()
        {
            _Client = DependencyService.Get<IFirebaseDBProvider>().GetClient();

            _Client.Child("Recruteurs").AsObservable<Recruteur>().Subscribe((ev) => {
                if(ev.Object == null)
                { return; }

                Recruteur rec = _Rec.FirstOrDefault(m => m.Key == ev.Key);

                if(ev.EventType == FirebaseEventType.InsertOrUpdate)
                {
                    if(rec == null)
                    {
                        Recruteur newR = new Recruteur
                        {
                            Key = ev.Key,
                            SocietyName = ev.Object.SocietyName,
                            CityWork = ev.Object.CityWork,
                            ActivitySector = ev.Object.ActivitySector,
                            Description = ev.Object.Description,
                            LogInfo = ev.Object.LogInfo
                        };
                        OnAdd?.Invoke(newR);
                    }
                }
            });
        }

        public void Add(Recruteur rec)
        {
            _Client.Child("Recruteurs").PostAsync(JsonConvert.SerializeObject(rec));
            _Rec.Add(rec);

        }

        public Task<List<Recruteur>> GetAllAsync()
        {
            throw new NotImplementedException();
        }
    }
}
