﻿using AdoptesUnDev.Models;
using AdoptesUnDev.Services;
using AdoptesUnDev.Views;
using System;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.StyleSheets;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace AdoptesUnDev
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();


            
            DependencyService.Register<FirebaseDBProvider>();
            DependencyService.Register<RegisterDevService>();
            DependencyService.Register<LoginService>();
            DependencyService.Register<RegisterRecService>();
            App.Current.Properties.TryGetValue("user_key", out object userKey);
            if (userKey != null)
            {
                MainPage = new Welcome();
            }
            else MainPage = new Welcome();

            Resources.Add(StyleSheet.FromAssemblyResource(
                IntrospectionExtensions.GetTypeInfo(typeof(Welcome)).
                Assembly, "AdoptesUnDev.Assets.style.css"));

        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
