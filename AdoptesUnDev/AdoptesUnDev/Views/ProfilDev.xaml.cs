using AdoptesUnDev.ViewModels;
using AdopteUnDev.ViewModels;
﻿using AdoptesUnDev.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdoptesUnDev.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilDev : ContentPage
    {
        public ProfilDev()
        {

            //BindingContext = new RegDevViewModel();

            BindingContext = new ViewModelDev();

            InitializeComponent();
        }
    }
}