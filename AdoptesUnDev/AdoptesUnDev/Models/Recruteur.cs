﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdoptesUnDev.Models
{
    class Recruteur
    {
        public string Key { get; set; }
        public string SocietyName { get; set; }
        public string Email { get; set; }
        public string CityWork { get; set; }
        public string ActivitySector { get; set; }
        public string Description { get; set; }
        public LoginInfo LogInfo { get; set; }
    }
}
