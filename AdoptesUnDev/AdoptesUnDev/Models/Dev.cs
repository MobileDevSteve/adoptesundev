﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdoptesUnDev.Models
{
    class Dev
    {
        public string Key { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public DateTime BirthDate { get; set; }
        public List<Skills> SkillsList { get; set; }
        public string FreeDesc { get; set; }
        public LoginInfo LogInfo { get; set; }


    }
}
