﻿namespace AdoptesUnDev.Models
{
    public class LoginInfo
    {
        public string Key { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}